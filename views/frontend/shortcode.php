<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 *
 * @var string $link
 * @var string $id
 *
 */

?>

<div class="passport-shortcode">
    <div class="passport-shortcode__content" data-shortcode>
        <span class="passport-shortcode__text"><?php esc_html_e( 'CANTIDAD', 'custom-checkout-plugin' ); ?></span>
        <button class="passport-shortcode__quantity-button" type="button" data-product-counter data-direction="minus"><?php echo snowpassport_svg('minus'); ?></button>
        <input type="number" name="quantity_of_passport_products" class="quantity_of_passport_products passport-shortcode__input" value="1" min="1" data-quantity-input>
        <button class="passport-shortcode__quantity-button" type="button" data-product-counter data-direction="plus"><?php echo snowpassport_svg('plus'); ?></button>
    </div>
    <div class="passport-shortcode__button-wrap">
        <button type="button" data-href="<?php echo $link; ?>" data-product-id="<?php echo $id; ?>" id="display_passport_product" class="product-passport__button product-passport__button--dark product-passport__button--small product-passport__button--shortcode"><?php esc_html_e( 'AÑADIR AL CARRITO', 'custom-checkout-plugin' ); ?></button>

    </div>

</div>
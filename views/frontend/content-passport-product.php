<?php

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
    echo get_the_password_form();
    return;
}
?>
<div class="col-12">
    <div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

        <div class="product-passport">
            <?php
            if ($_GET['total'] == 1){
                echo '';
            } else { ?>
            <div class="product-passport-progress">
                <?php

                for($i=0; $i < $_GET['total']; $i++) 
                    {
                        ?>
                        <div class="product-passport-progress__item <?php if($i == ($_GET['total'] - $_GET['quantity'])){ echo 'is-active'; } ?> ">
                            <div class="product-passport-progress__circle"></div>
                            <span class="product-passport-progress__text">
                                <?php esc_html_e('PASAPORTE ', 'custom-checkout-plugin');
                                echo $i+1; ?>
                            </span>
                        </div>
                    <?php } ?>
                    </div>
            <?php } ?>

            <form class="passport-cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>

                <?php do_action( 'passport_woocommerce_after_single_product_summary' ); ?>

            </form>

            <div class="passport-modal" data-modal-empty>
                <div class="passport-modal__content">
                    <i class="passport-modal__close" data-modal-empty-close><?php echo snowpassport_svg('close'); ?></i>
                    <h2 class="passport-modal__title"><?php esc_html_e('¿Estás seguro de que no quieres personalizar tu pasaporte gratis?', 'custom-checkout-plugin'); ?></h2>
                    <button type="button" class="product-passport__button product-passport__button--dark " data-modal-empty-close>
                        <span><?php esc_html_e('PERSONALIZAR GRATIS', 'custom-checkout-plugin'); ?></span>
                    </button>
                    <button type="button" class="product-passport__button " data-modal-empty-continue>
                        <span><?php esc_html_e('CONTINUAR SIN PERSONALIZAR', 'custom-checkout-plugin'); ?></span>
                    </button>
                </div>
            </div>

        </div>

    </div>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
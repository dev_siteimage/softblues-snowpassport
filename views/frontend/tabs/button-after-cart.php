<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 *
 * @var string $link
 *
 */

?>

<a href="<?php echo $link; ?>" class="product-passport__button active">
    <span><?php esc_html_e( '+ AÑADIR PASAPORTE', 'custom-checkout-plugin' ); ?></span>
</a>
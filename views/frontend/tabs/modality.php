<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

?>
<strong class="product-passport__title"><?php esc_html_e('Personaliza los datos de tu pasaporte gratis', 'custom-checkout-plugin') ?></strong>
<strong class="product-passport__subtitle"><?php esc_html_e('Modalidad', 'custom-checkout-plugin') ?></strong>
<fieldset id="group-modality" class="product-modality">
    <label class="product-modality__label">
        <input class="input-hidden" type="radio" name="group-modality" value="esquí" checked="checked">
        <div class="item-label product-modality__item">
            <?php echo snowpassport_svg('skiing-icon'); ?>
            <span><?php esc_html_e( 'Esquí', 'custom-checkout-plugin' ); ?></span>
        </div>
    </label>
    <label class="product-modality__label">
        <input class="input-hidden" type="radio" name="group-modality" value="snow" >
        <div class="item-label product-modality__item">
            <?php echo snowpassport_svg('snowboard-icon'); ?>
            <span><?php esc_html_e( 'Snow', 'custom-checkout-plugin' ); ?></span>
        </div>
    </label>
</fieldset>
<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * @var string $link
 */

?>

<div class="img-wrapper" id="sticker">
    <?php do_action( 'woocommerce_before_single_product_summary' ); ?>
    <input type="hidden" id="upload_passport_image_input" name="upload-passport-image">
</div>
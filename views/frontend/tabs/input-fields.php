<?php       

if ( ! defined( 'WPINC' ) ) {
    die;
}

?>

<div class="product-info">
    <div class="product-info__item">
        <input type="text" name="first-name" class="first-name product-info__input" placeholder="<?php esc_html_e( 'Nombre', 'custom-checkout-plugin' ); ?>">
    </div>
    <div class="product-info__item">
        <input type="text" name="second-name" class="second-name product-info__input" placeholder="<?php esc_html_e( 'Apellidos', 'custom-checkout-plugin' ); ?>">
    </div>
    <div class="product-info__item">
        <input type="text" name="season" class="season product-info__input" placeholder="<?php esc_html_e( 'Temporada', 'custom-checkout-plugin' ); ?>">
    </div>
    <div class="product-info__item">
        <input type="text" name="nationality" class="nationality product-info__input" placeholder="<?php esc_html_e( 'Nacionalidad', 'custom-checkout-plugin' ); ?>">
    </div>
    
</div>
<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

?>

<strong class="product-passport__title"><?php esc_html_e('Elige el color de la linterna', 'custom-checkout-plugin') ?></strong>
<div class="product-passport__buttons">
    <button type="button" class="product-passport__button active svg-color svg-color--button svg-color--aleatorio" data-color-button="aleatorio">
        <span><?php esc_html_e('Color aleatorio', 'custom-checkout-plugin'); ?></span>
    </button>
    <div class="product-passport__buttons-wrapper">
        <button type="button" class="product-passport__button svg-color svg-color--button svg-color--dorado" data-color-button="dorado">
            <span><?php esc_html_e('Dorado', 'custom-checkout-plugin'); ?></span>
        </button>
        <button type="button" class="product-passport__button svg-color svg-color--button svg-color--metal" data-color-button="metal">
            <span><?php esc_html_e('Metal', 'custom-checkout-plugin'); ?></span>
        </button>
        <button type="button" class="product-passport__button svg-color svg-color--button svg-color--rosa" data-color-button="rosa">
            <span><?php esc_html_e('Rosa', 'custom-checkout-plugin'); ?></span>
        </button>
        <button type="button" class="product-passport__button svg-color svg-color--button svg-color--azul" data-color-button="azul">
            <span><?php esc_html_e('Azul', 'custom-checkout-plugin'); ?></span>
        </button>
    </div>
    <input type="text" value="aleatorio" name="color-picker" class="color-picker input-hidden" placeholder="<?php esc_html_e( 'Color Picker Input', 'custom-checkout-plugin' ); ?>">
    <br>
</div>

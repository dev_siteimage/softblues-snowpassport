<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * @var string $id
 */

$edit = ! empty( $_REQUEST['passport-edit'] ) ? esc_html__( 'APLICAR CAMBIOS', 'custom-checkout-plugin' ) : esc_html__( 'AÑADIR AL CARRITO', 'custom-checkout-plugin' );

?>

<button type="submit" name="add-to-cart" data-validate="true" value="<?php echo $id; ?>" class="single_add_to_cart_button button alt product-info__button"><?php echo $edit; ?></button>
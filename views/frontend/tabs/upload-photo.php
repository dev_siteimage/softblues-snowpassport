<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

?>

<strong class="product-passport__subtitle"><?php esc_html_e('Datos del pasaporte', 'custom-checkout-plugin') ?></strong>

<label class="product-passport__upload" data-upload-item>
    <input type="file" class="input-hidden" id="async-upload" name="async-upload">
    <input type="hidden" class="input-hidden" id="upload-passport-image-original" name="upload-passport-image-original">
    <div class="product-passport__upload-item">
        <?php echo snowpassport_svg('upload-icon'); ?>
        <span><?php esc_html_e( 'Sube tu foto', 'custom-checkout-plugin' ); ?></span>
    </div>
    <span class="product-passport__upload-item-text"><?php esc_html_e( 'Tamaño del archivo máximo 20MB', 'custom-checkout-plugin' ); ?></span>
</label>

<div class="cropped">
    <button type="button" class="cropped__close" data-remove-img><?php echo snowpassport_svg('close'); ?></button>
    <div class="cropped__img">
        <img class="cropped__image" src="" alt="alt">

    </div>
    <div class="cropped__buttons">
        <button type="button" class="product-passport__button product-passport__button--small" data-edit-image>
            <?php echo snowpassport_svg('crop-icon'); ?>
            <span><?php esc_html_e('Editar foto', 'custom-checkout-plugin'); ?></span>
        </button>
        <label class="product-passport__upload">
            <input type="file" class="input-hidden" id="async-upload-new" name="async-upload">
            <div class="product-passport__upload-item product-passport__upload-item--small">
                <?php echo snowpassport_svg('upload-icon'); ?>
                <span><?php esc_html_e( 'Subir nueva foto', 'custom-checkout-plugin' ); ?></span>
            </div>
        </label>
    </div>
    <div class="cropped__loading">
        <span><?php _e('Cargando...', 'custom-checkout-plugin') ?></span>
    </div>
</div>


<div class="canvas-wrapp canvas-wrapp--main">
    <div class="canvas-inner">
        <h2 class="title"><?php esc_html_e( 'Edita tu foto:', 'custom-checkout-plugin' ); ?></h2>
        <div class="js-avatar-preview-wrapper">
            <img style="width: 271px; height: 271px;" class="js-avatar-preview" src="" alt="Passporte foto">
        </div>
        <i class="close"><?php echo snowpassport_svg('close'); ?></i>
        <div class="btn-wrapp">
            <input type="button" class="product-passport__button product-passport__button--dark"
                id="get-cropped-image-src" value="<?php esc_html_e( 'Guadar recorte', 'custom-checkout-plugin' ); ?>">
        </div>
    </div>
</div>
<div class="canvas-wrapp canvas-wrapp--notification">
    <div class="canvas-inner">
        <h2 class="title">
            <?php esc_html_e( 'La foto que intentas subir es demasiado grande, por favor sube una mas pequeña', 'custom-checkout-plugin' ); ?>
        </h2>
        <i class="close"><?php echo snowpassport_svg('close'); ?></i>
    </div>
</div>
<div class="canvas-wrapp canvas-wrapp--load">
    <div class="canvas-inner">
        <h2 class="title">
            <?php _e('Cargando...', 'custom-checkout-plugin') ?>    
        </h2>
    </div>
</div>
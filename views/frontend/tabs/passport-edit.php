<?php

if ( ! defined( 'WPINC' ) ) {
    die;
}

$edit = ! empty( $_REQUEST['passport-edit'] ) ? $_REQUEST['passport-edit'] : '';

?>

<input type="hidden" name="passport-edit" class="passport-edit" value="<?php echo $edit; ?>">
<br>
<input type="hidden" name="passport-image-id" class="passport-image-id" value="<?php echo uniqid(); ?>">
<br>
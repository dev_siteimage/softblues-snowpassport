(function($) {
    $(document).ready(function() {

        if (typeof(passportAttributes) != "undefined" && passportAttributes !== null) {
            let passport = passportAttributes;
            let form = $('form.passport-cart');
            let fields = [
                'passport-edit',
                'passport-image-id',
                'upload-passport-image',
                'upload-passport-image-original',
                'color-picker',
                'group-modality',
                'first-name',
                'second-name',
                'season',
                'nationality'
            ];
            if (form.length) {
                for (let [key, val] of Object.entries(passport.data)) {
                    if ($.inArray(key, fields) !== -1) {

                        if (key == 'group-modality') {
                            let inputs = form.find(`#${key} input`);
                            inputs.removeAttr('checked');
                            for (i = 0; i < inputs.length; i++) {
                                let input = $(inputs[i]);
                                if (input.val() == val) {
                                    input.prop('checked', true);
                                }
                            }
                        } else if (key == 'color-picker') {
                            form.find('.product-passport__buttons button[type="button"]').removeClass('active');
                            form.find(`button[data-color-button="${val}"]`).addClass('active');
                            form.find(`input[name="${key}"]`).attr('value', val);
                        } else {
                            form.find(`input[name="${key}"]`).val(val);
                        }

                        if (key == 'upload-passport-image' && passport.data['upload-passport-image'] != '') {
                            form.find(`div.cropped`).addClass('active');
                            form.find(`img.cropped__image`).attr('src', val);
                        }
                    }
                }
            }
        }

        $('#display_passport_product').on('click', function(event) {
            event.preventDefault();
            let $this = $(this);
            let id = $this.data('product-id');
            let quantity = $this.closest('.passport-shortcode').find('.quantity_of_passport_products').val();

            let url = buildUrl($this.data('href'), 'id', id);
            url = buildUrl(url, 'quantity', quantity);
            url = buildUrl(url, 'total', quantity);

            window.location.href = url;
        });

        function buildUrl(base, key, value) {
            let sep = (base.indexOf('?') > -1) ? '&' : '?';
            return base + sep + key + '=' + value;
        }

        function loadImage() {
            let files = this.files;
            let maxFileSize = 20971520; // 20MB in bytes
            if (files[0].size > maxFileSize) {
                this.value = "";
                $('.canvas-wrapp--notification').fadeIn();
            } else {
                let photo = files[0];

                let data = new FormData();
                data.append('action', 'upload_passport_image_original');
                data.append('image', photo);

                $.ajax({
                    url: wpData.ajaxUrl,
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    beforeSend: function(data) {
                        $('.canvas-wrapp--load').fadeIn();
                    },
                    success: function(data, textStatus, jqXHR) {

                        if( typeof data.error != 'undefined' ) {
                            console.log(data.error);
                        } else {
                            $('#upload-passport-image-original').val( data.url );

                            let reader = new FileReader();
                            reader.onload = function(event) {
                                let image = $('.js-avatar-preview');
                                image.attr('src', event.target.result);
                                image.cropper("destroy");

                                image.cropper({
                                    viewMode: 1,
                                    aspectRatio: 3 / 4,
                                    movable: true,
                                    ready: function() {},
                                    built: function() {
                                        image.cropper('setCropBoxData', {
                                            width: 100,
                                            height: 100
                                        });
                                    }
                                });
                                $('.canvas-wrapp--load').fadeOut();
                                $('.canvas-wrapp--main').fadeIn();
                            };
                            reader.readAsDataURL(photo);
                        }
                    }
                });
            }
        }

        $(document).on('click', '[data-edit-image]', function() {
            let image = $('.js-avatar-preview');
            let localSrc = $('#upload-passport-image-original').val();

            $('.canvas-wrapp--main').fadeIn();

            image.attr('src', localSrc);
            image.cropper("destroy");
            image.cropper({
                viewMode: 1,
                aspectRatio: 3 / 4,
                movable: true,
                ready: function() {},
                built: function() {
                    image.cropper('setCropBoxData', {
                        width: 100,
                        height: 100
                    });
                }
            });
        });

        $('#async-upload').on('change', loadImage);
        $('#async-upload-new').on('change', loadImage);

        $('#get-cropped-image-src').on('click', getCroppedImageSrc);

        function getCroppedImageSrc(event) {
            closeCanvas();
            let image = $('.js-avatar-preview');
            const canvas = image.cropper('getCroppedCanvas', {
                imageSmoothingEnabled: true,
                imageSmoothingQuality: 'high',
            })
            const base64encodedImage = canvas.toDataURL("image/jpeg", 1);
            let data = new FormData();
            data.append('action', 'upload_passport_image');
            data.append('image', base64encodedImage);

            $.ajax({
                url: wpData.ajaxUrl,
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function(data) {
                    $('.cropped').addClass('is-active');
                },
                success: function(data, textStatus, jqXHR) {
                    $('#upload_passport_image_input').attr('value', data.url);
                    $('.cropped__image').attr('src', data.url);
                    $('.cropped').addClass('active');
                    $('.cropped').removeClass('is-active');
                    $('[data-upload-item]').css({ 'display': 'none' });
                }
            });
        };

        $('.close').on('click', closeCanvas);

        function closeCanvas(event) {
            $('.canvas-wrapp').fadeOut();
        }

        $(document).on('click', '[data-color-button]', function() {
            $('[data-color-button]').removeClass('active');
            $(this).addClass('active');

            let colorValue = $(this).attr('data-color-button');
            $('.color-picker').attr('value', colorValue)
        });

        $(document).on('click', '[data-product-counter]', function(e) {
            e.preventDefault();
            var th = $(this);
            var direction = th.attr('data-direction');
            var directionWrap = th.closest('[data-shortcode]');
            var quantityInput = directionWrap.find('[data-quantity-input]');
            var currentValue = +quantityInput.val();
            var newValue;

            if (direction === 'plus') {
                newValue = currentValue + 1;
                quantityInput.val(newValue);
                if (newValue >= 3) {
                    newValue = currentValue - 3 >= 3 ? currentValue - 3 : 3;
                    quantityInput.val(newValue);
                }
            } else {
                newValue = currentValue - 1 >= 1 ? currentValue - 1 : 1;
                quantityInput.val(newValue);
            }

        });

        const smallDevice = window.matchMedia("(min-width: 768px)");

        smallDevice.addListener(handleDeviceChange);

        function handleDeviceChange(e) {
            if (e.matches) {
                $("#sticker").stick_in_parent({
                    offset_top: 20
                });

            } else {
                $("#sticker").trigger("sticky_kit:detach");
            }
        }
        handleDeviceChange(smallDevice);

        $(document).on('click', '[data-remove-img]', function() {
            $(this).closest('.cropped').removeClass('active');
            $(this).closest('.cropped').find('.cropped__image').attr('src', $('.product-passport__image').attr('src'));
            $('.product-passport__upload').css('display', 'block');
            $('#async-upload').attr('value', '');

            $.ajax({
                url: wpData.ajaxUrl,
                type: 'POST',
                data: {
                    action: 'delete_passport_image',
                    imgSrc: $('.cropped__image').attr('src')
                },
                success: function(data) {}
            });
        });

        $('.single_add_to_cart_button').on('click', function(event) {
            let $this = $(this);
            let validate = $this.data('validate');
            let form = $this.closest('form.passport-cart');
            let isEmpty = true;
            let result = true;

            if (form.length) {
                let fields = [
                    'upload-passport-image',
                    'first-name',
                    'second-name',
                    'season',
                    'nationality'
                ];

                $.each(fields, function(index, value) {
                    if (form.find(`input[name="${value}"]`).val() != '') {
                        isEmpty = false;
                    }
                });
            }

            if (isEmpty && validate) {
                result = $('[data-modal-empty]').fadeIn();
                event.preventDefault();
            }

            if (validate && !result) {
                event.preventDefault();
            }
        });

        $(document).on('click', '[data-modal-empty-continue]', function(event) {
            let form = $("form.passport-cart");
            let button = form.find('.single_add_to_cart_button');
            button.data('validate', false);
            $(this).closest('[data-modal-empty]').fadeOut();
            button.click();
        });
        $(document).on('click', '[data-modal-empty-close]', function() {
            $(this).closest('[data-modal-empty]').fadeOut();
        });
        $(window).on('load', function() {
            $('.product-passport-progress__item.is-active').prevAll().addClass('active is-active');
        })

    });
})(jQuery);
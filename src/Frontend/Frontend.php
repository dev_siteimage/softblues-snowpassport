<?php namespace CustomCheckout\Frontend;

use Premmerce\SDK\V2\FileManager\FileManager;
use CustomCheckout\Admin\Admin;
use WP_Query;

/**
 * Class Frontend
 *
 * @package CustomCheckout\Frontend
 */
class Frontend
{
    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * Id of simple product which add to cart button need replace
     */
    const ID_SIMPLE_PRODUCT = 402;

    /**
     * Plugin version
     */
    const VERSION = '1.04';

    /**
     * @var $fields
     */
    private $fields;

    public function __construct( FileManager $fileManager )
    {
        $this->fileManager = $fileManager;

        $this->fields = array(
          'passport-image-id',
          'color-picker',
          'upload-passport-image',
          'upload-passport-image-original',
          'first-name',
          'second-name',
          'season',
          'nationality',
          'group-modality',
        );

        add_filter( 'init', [$this, 'writeToSession'], 10 );
        add_action( 'wp_enqueue_scripts', [$this, 'enqueueScripts'] );
        add_filter( 'wc_get_template_part', [$this, 'customWcTemplatePart'], 10, 3 );
        add_action( 'woocommerce_single_product_summary', [$this, 'removeActionsWooSingleProductSummary'], 0 );
        add_action( 'passport_woocommerce_after_single_product_summary', [$this, 'addCustomFieldsToProduct'], 20 );
        add_action( 'woocommerce_add_cart_item_data', [$this, 'addToCartCustomItemData'], 20, 4 );
        add_action( 'woocommerce_add_order_item_meta', [$this, 'addToOrderCustomItemData'], 20, 2 );
        add_action( 'woocommerce_cart_item_thumbnail', [$this, 'uniqProductThumbInCart'], 20, 3 );
        add_action( 'woocommerce_cart_item_name', [$this, 'displayInCartCustomItemData'], 20, 3 );
        add_filter( 'woocommerce_cart_item_data_to_validate', [$this, 'validateCartItemData'], 10, 2 );
        add_filter( 'woocommerce_add_to_cart_redirect', [$this, 'checkProductQuantity'], 10, 2 );
        add_filter( 'wc_get_template', [$this, 'returnCartTemplate'], 999, 2 );
        add_action( 'woocommerce_after_cart_contents', [$this, 'renderButtonAfterCartContents'], 999 );
        add_action( 'wc_add_to_cart_message_html', [$this, 'filterAddToCartmessage'], 20 );
        add_filter( 'woocommerce_placeholder_img_src', [$this, 'changeDefaultWCPlaceholderImg'], 20 );
        add_filter( 'woocommerce_order_item_get_formatted_meta_data', [$this, 'filterFormattedMetaData'], 20, 2 );
        add_action( 'woocommerce_remove_cart_item', [$this, 'deleteImagesIfRemoveCartItem'], 10, 2 );
    }

  public function deleteImagesIfRemoveCartItem( $itemKey, $cart )
  {
    if ( ! empty( $_REQUEST['passport-edit'] ) && $_REQUEST['passport-edit'] == $itemKey ) {
      return;
    }

    $imagesKey = ['upload-passport-image', 'upload-passport-image-original'];
    foreach ( $imagesKey as $image ) {
      if ( ! empty( $cart->cart_contents[$itemKey][$image] ) ) {
        $uploadDir = wp_get_upload_dir();
        if ( file_exists( $uploadDir['path'] . '/' . basename( $cart->cart_contents[$itemKey][$image] ) ) ) {
          unlink( $uploadDir['path'] . '/' . basename( $cart->cart_contents[$itemKey][$image] ) );
        }
      }
    }
  }

  public function filterFormattedMetaData( $formattedMetaData, $scope )
    {
        foreach ( $formattedMetaData as $key => $val ) {
            if( $val->key == 'Upload passport image' ) {
                unset( $formattedMetaData[$key] );
            }
        }

        return $formattedMetaData;
    }

    public function changeDefaultWCPlaceholderImg( $src )
    {
        global $product;
        if( $this->isPassport( $product->get_id() ) ) {
            return $this->fileManager->locateAsset('frontend/images/default-photo.jpg');
        }

        return $src;
    }

    public function returnCartTemplate( $template, $templateName )
    {
        if ( $templateName == 'cart/cart.php' ) {
            return $this->fileManager->locateTemplate( 'frontend/cart.php' );
        }

        return $template;
    }

    public function renderButtonAfterCartContents()
    {
        $cart       = WC()->session->get( 'cart', null );
        $passportId = self::getFirstPassportProductId();
        $iSpassport = false;
        $quantity   = 0;

        if( $cart ) {
            foreach ( $cart as $key => $values ) {
                if( $values['product_id'] == $passportId ) {
                    $iSpassport = true;
                    $quantity++;
                }
            }
        }

        $quantity = ( $quantity == 1 ) ? 2 : 1;

        if( $iSpassport ) {
            ob_start();
                echo $this->buttonAfterCartContents( add_query_arg( array( 'id' => $passportId, 'quantity' => 1, 'total' => 1 ), get_the_permalink( $passportId ) ) );
                $content = ob_get_contents();
            ob_end_flush();
            return $content;
        }
    }


    public function checkProductQuantity( $url, $addingToCart )
    {
        if( $addingToCart ) {
            if ( ! $this->isPassport( $addingToCart->get_id() ) ) {
                return $url;
            }

            if( session_status() === PHP_SESSION_ACTIVE ) {
                if( ! empty( $_SESSION[Admin::PRODUCT_TYPE . '-quantity'] ) ) {
                    if( $_SESSION[Admin::PRODUCT_TYPE . '-quantity'] > 1 ) {
                        $_SESSION[Admin::PRODUCT_TYPE . '-quantity'] = absint( $_SESSION[Admin::PRODUCT_TYPE . '-quantity'] ) - 1;
                        $redirect = add_query_arg( array( 'quantity' => $_SESSION[Admin::PRODUCT_TYPE . '-quantity'], 'total' => $_SESSION[Admin::PRODUCT_TYPE . '-total'] ), get_the_permalink( $_SESSION[Admin::PRODUCT_TYPE . '-id'] ) );
                        
                        wp_safe_redirect( $redirect );
                        exit;
                    } else {
                        wp_safe_redirect( wc_get_cart_url() );
                        exit;
                    }
                }
            }

            wp_safe_redirect( wc_get_cart_url() );
            exit;
        }

        return $url;
    }

    public function writeToSession()
    {
        if ( session_status() === PHP_SESSION_NONE ) {
            session_start();
        }

        if( ! empty( $_GET['id'] ) && ! empty( $_GET['quantity'] ) && $_GET['id'] == self::getFirstPassportProductId() ) {
            $_SESSION[Admin::PRODUCT_TYPE . '-quantity'] = absint( $_GET['quantity'] );
            $_SESSION[Admin::PRODUCT_TYPE . '-id'] = absint( $_GET['id'] );

            if( ! empty( $_GET['total'] ) ) {
                $_SESSION[Admin::PRODUCT_TYPE . '-total'] = absint( $_GET['total'] );
            }
        }
    }

    /**
     * Add scripts
     */
    public function enqueueScripts()
    {
        wp_enqueue_style( 'custom-checkout-cropper-style', $this->fileManager->locateAsset( 'frontend/css/cropper.min.css' ), array(), self::VERSION );
        wp_enqueue_style( 'custom-checkout-frontend-style', $this->fileManager->locateAsset( 'frontend/css/style.css' ), array(), self::VERSION );

        wp_enqueue_script( 'custom-checkout-sticky-script', $this->fileManager->locateAsset( 'frontend/js/jquery.sticky-kit.js' ), array('jquery'), self::VERSION );
        wp_enqueue_script( 'custom-checkout-cropper-script', $this->fileManager->locateAsset( 'frontend/js/cropper.min.js' ), array('jquery'), self::VERSION );
        wp_enqueue_script( 'custom-checkout-jquery-cropper-script', $this->fileManager->locateAsset( 'frontend/js/jquery-cropper.min.js' ), array('jquery'), self::VERSION );
        wp_enqueue_script( 'custom-checkout-frontend-script', $this->fileManager->locateAsset( 'frontend/js/common.js' ), array('jquery'), self::VERSION );

        $wpData = array(
          'ajaxUrl' => admin_url( 'admin-ajax.php' ),
        );
        wp_localize_script( 'custom-checkout-frontend-script', 'wpData', $wpData );

        if( ! empty( $_REQUEST['passport-edit'] ) ) {
            if ( ! $this->isPassport( get_the_ID() ) ) {
                return;
            }

            $cart = WC()->session->get( 'cart', null );
            if( $cart ) {
                foreach ( $cart as $key => $values ) {
                    if ( $_REQUEST['passport-edit'] == $key ) {

                        $passportAttributes = array(
                          'data' => $values,
                        );
                        wp_localize_script( 'custom-checkout-frontend-script', 'passportAttributes', $passportAttributes );

                    }
                }
            }
        }
    }

    public function customWcTemplatePart( $template, $slug, $name )
    {
        if ( get_the_ID() == self::getFirstPassportProductId() && $slug == 'content' && $name == 'single-product' ) {
            return $this->fileManager->locateTemplate( 'frontend/content-passport-product.php' );
        }

        return $template;
    }

    public function removeActionsWooSingleProductSummary()
    {
        global $product;
        if( $product->get_id() == self::ID_SIMPLE_PRODUCT ) {
            remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
            add_action( 'woocommerce_single_product_summary', [$this, 'replaceSimpleProductAddToCartBtn'], 30 );
        }

        if ( ! $this->isPassport( get_the_ID() ) ) {
            return;
        }

        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
    }

    public function replaceSimpleProductAddToCartBtn()
    {
        ob_start();
        $productId = Frontend::getFirstPassportProductId();
        echo $this->fileManager->renderTemplate('frontend/shortcode.php', array(
          'link' => get_the_permalink( $productId ),
          'id'   => $productId,
        ));
        $content = ob_get_contents();
        ob_end_flush();
        return $content;
    }

    public function addCustomFieldsToProduct()
    {
        if ( ! $this->isPassport( get_the_ID() ) ) {
            return;
        }

        ob_start();

            echo '<div class="product-passport__wrapper">';
                echo '<div class="product-passport__img">';
                    echo $this->customCheckoutRenderProductEdit();
                    echo $this->customCheckoutRenderImage();
                echo '</div>';
                echo '<div class="product-passport__content">';
                    echo $this->customCheckoutRenderColorPicker();
                    echo $this->customCheckoutRenderModality();
                    echo $this->customCheckoutRenderUploadPhoto();
                    echo $this->customCheckoutRenderInputFields();
                    echo $this->customCheckoutRenderAddToCartBtn();
                echo '</div>';
            echo '</div>';

            $content = ob_get_contents();

        ob_end_flush();
        return $content;
    }

    public function addToCartCustomItemData( $cartItemData, $productId, $variationId, $quantity )
    {
        foreach ( $this->fields as $field ) {
            if( isset( $_REQUEST[$field] ) && $_REQUEST[$field] != '' ) {
                $cartItemData[$field] = sanitize_text_field( $_REQUEST[$field] );
            }
        }

        return $cartItemData;
    }

    public function addToOrderCustomItemData( $itemId, $values )
    {
        foreach ( $this->fields as $field ) {
            if ( $field == 'passport-image-id' ) {
                continue;
            }
            if ( $field == 'upload-passport-image-original' ) {
                $uploadDir = wp_get_upload_dir();
                if ( file_exists( $uploadDir['path'] . '/' . basename( $values[$field] ) ) ) {
                    unlink( $uploadDir['path'] . '/' . basename( $values[$field] ) );
                }
                continue;
            }
            if ( ! empty( $values[$field] ) ) {
                wc_add_order_item_meta( $itemId, ucfirst( str_replace( '-', ' ', $field ) ), $values[$field] );
            }
        }
    }

    public function uniqProductThumbInCart( $productImg, $cartItem, $cartItemKey )
    {
        $productImg = get_the_post_thumbnail( $cartItem['product_id'] );
        if( ( ! $productImg || $productImg == '' ) && $cartItem['product_id'] == self::getFirstPassportProductId() ) {
            $defaultPhoto = $this->fileManager->locateAsset('frontend/images/default-photo.jpg');
            $productImg = '<img src="' . $defaultPhoto . '">';
        }

        return $productImg;
    }

    public function displayInCartCustomItemData( $productName, $cartItem, $cartItemKey )
    {
        $isPasport = false;
        $fields    = '';
        foreach ( $this->fields as $field ) {
            if ( in_array( $field, ['passport-image-id', 'upload-passport-image-original'] ) ) {
                continue;
            }
            if( $field == 'upload-passport-image' ) {
                if ( ! empty( $cartItem[$field] ) ) {
                    $isPasport = true;
                }
                continue;
            }
            if ( ! empty( $cartItem[$field] ) ) {
                $isPasport = true;
                $colorClass = $field == 'color-picker' ? 'svg-color  svg-color--'. $cartItem[$field] : '';
                $fields .= '<div class="' . $field . '">' . ucfirst( str_replace( '-', ' ', $field ) ) . ': <span class="' . $colorClass . '">' . $cartItem[$field] . '</span></div>';
            }
        }

        if( $isPasport ) {
            $link = add_query_arg( array( 'passport-edit' => $cartItemKey ), get_the_permalink( $cartItem['product_id'] ) );
            $fields .= '<a href="' . $link . '" class="edit-product">' .  esc_attr__( 'Edit', 'custom-checkout-plugin' ) . '</a>';
        }

        return $productName . $fields;
    }

    public static function getFirstPassportProductId()
    {
        $args = [
          'post_type' => 'product',
          'tax_query' => [
            [
              'taxonomy' => 'product_type',
              'field'    => 'slug',
              'terms'    => [Admin::PRODUCT_TYPE],
            ],
          ],
        ];

        $query = new WP_Query( $args );

        if ( ! empty( $query->posts ) ) {
            return $query->posts[0]->ID;
        }

        return false;
    }

    public function validateCartItemData( $array, $product )
    {
        if( ! empty( $_REQUEST['passport-edit'] ) ) {
            $cart = WC()->session->get( 'cart', null );
            if( $cart ) {
                foreach ( $cart as $key => $values ) {
                    if( $_REQUEST['passport-edit'] == $key ) {
                        WC()->cart->remove_cart_item( $key );
                    }
                }
            }
        }

        if ( $array['type'] == Admin::PRODUCT_TYPE ) {
            return array(
              'type'       => Admin::PRODUCT_TYPE,
              'attributes' => '',
            );
        }

        return $array;
    }

    public function filterAddToCartmessage( $message )
    {
        $all_notices = WC()->session->get( 'wc_notices', [] );

        foreach ( $all_notices['success'] as $notice ) {
            if( $notice['notice'] == $message ) {
                return '';
            }
        }
        return $message;
    }
    
    protected function isPassport( $id )
    {
        if( self::getFirstPassportProductId() == $id ) {
            return true;
        }

        return false;
    }

    protected function customCheckoutRenderProductEdit()
    {
        return $this->fileManager->renderTemplate( 'frontend/tabs/passport-edit.php' );
    }

    protected function customCheckoutRenderImage()
    {
        return $this->fileManager->renderTemplate( 'frontend/tabs/image.php');
    }

    protected function customCheckoutRenderColorPicker()
    {
        return $this->fileManager->renderTemplate( 'frontend/tabs/color-picker.php' );
    }

    protected function customCheckoutRenderModality()
    {
        return $this->fileManager->renderTemplate( 'frontend/tabs/modality.php' );
    }

    protected function customCheckoutRenderUploadPhoto()
    {
        return $this->fileManager->renderTemplate( 'frontend/tabs/upload-photo.php');
    }

    protected function customCheckoutRenderInputFields()
    {
        return $this->fileManager->renderTemplate( 'frontend/tabs/input-fields.php' );
    }

    protected function customCheckoutRenderAddToCartBtn()
    {
        return $this->fileManager->renderTemplate( 'frontend/tabs/add-to-cart.php', array(
          'id' => self::getFirstPassportProductId(),
        ) );
    }

    protected function buttonAfterCartContents( $link )
    {
        return $this->fileManager->renderTemplate( 'frontend/tabs/button-after-cart.php', array(
          'link' => $link,
        ) );
    }
}
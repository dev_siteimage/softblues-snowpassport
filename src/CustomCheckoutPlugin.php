<?php namespace CustomCheckout;

use Premmerce\SDK\V2\FileManager\FileManager;
use CustomCheckout\Admin\Admin;
use CustomCheckout\Frontend\Frontend;
use CustomCheckout\Includes\WcProductPassport;

/**
 * Class CustomCheckoutPlugin
 *
 * @package CustomCheckout
 */
class CustomCheckoutPlugin
{
    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * CustomCheckoutPlugin constructor.
     *
     * @param string $mainFile
     */
    public function __construct( $mainFile )
    {
        $this->fileManager = new FileManager( $mainFile );

        add_action( 'plugins_loaded', [$this, 'loadTextDomain'] );
        add_action( 'init', [$this, 'registerShortcode'] );
        add_action( 'plugins_loaded', function () {
            add_filter( 'product_type_selector', [$this, 'addProductType'] );
            add_filter( 'woocommerce_product_class', [$this, 'loadProductTypePassport'], 10, 2 );
        } );
    }

    /**
     * Load Product Type Passport
     *
     * @param string $className
     * @param string $productType
     *
     * @return string
     */
    public function loadProductTypePassport( $className, $productType )
    {
        if( $productType == Admin::PRODUCT_TYPE ) {
            return WcProductPassport::class;
        }

        return $className;
    }

    /**
     * Add Product Type Passport
     *
     * @param array $types
     * @return void
     */
    public function addProductType( $types )
    {
        $types[Admin::PRODUCT_TYPE] = __( 'Passport', 'custom-checkout-plugin' );

        return $types;
    }

    /**
     * Run plugin part
     */
    public function run()
    {
        if ( is_admin() ) {
            new Admin( $this->fileManager );
        } else {
            new Frontend( $this->fileManager );
        }
    }

    /**
     * Load plugin translations
     */
    public function loadTextDomain()
    {
        $name = $this->fileManager->getPluginName();
        load_plugin_textdomain( 'custom-checkout-plugin', false, $name . '/languages/' );
    }

    /**
     * Register shortcode
     */
    public function registerShortcode()
    {
        add_shortcode('display_passport_product', function ($atts = [], $content = null) {
            $productId = Frontend::getFirstPassportProductId();
            return $this->fileManager->renderTemplate('frontend/shortcode.php', array(
                'link' => get_the_permalink( $productId ),
                'id'   => $productId,
            ));
        });
    }

    /**
     * Fired when the plugin is activated
     */
    public function activate()
    {
       // TODO: Implement activate() method.
    }

    /**
     * Fired when the plugin is deactivated
     */
    public function deactivate()
    {
        // TODO: Implement deactivate() method.
    }

    /**
     * Fired during plugin uninstall
     */
    public static function uninstall()
    {
        // TODO: Implement uninstall() method.
    }
}
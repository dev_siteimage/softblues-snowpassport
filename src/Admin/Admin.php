<?php namespace CustomCheckout\Admin;

use Premmerce\SDK\V2\FileManager\FileManager;
use Exception;

/**
 * Class Admin
 *
 * @package CustomCheckout\Admin
 */
class Admin
{
    /**
     * Ajax action handler
     */
    const AJAX_UPLOAD_IMAGE_ORIGINAL = 'upload_passport_image_original';
    const AJAX_UPLOAD_IMAGE          = 'upload_passport_image';
    const AJAX_DELETE_IMAGE          = 'delete_passport_image';

    /**
     * Product type
     */
    const PRODUCT_TYPE = 'passport';

    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * Admin constructor.
     *
     * Register menu items and handlers
     *
     * @param FileManager $fileManager
     */
    public function __construct( FileManager $fileManager )
    {
        $this->fileManager = $fileManager;

        add_filter( 'admin_footer', [$this, 'enableRegPriceForPassportProduct'] );
        add_filter( 'woocommerce_product_options_general_product_data', [$this, 'productAddCustomSettings'] );

        add_action( 'wp_ajax_' . self::AJAX_UPLOAD_IMAGE_ORIGINAL, [$this, 'ajaxUploadPassportImageOriginalSize'] );
        add_action( 'wp_ajax_nopriv_' . self::AJAX_UPLOAD_IMAGE_ORIGINAL, [$this, 'ajaxUploadPassportImageOriginalSize'] );

        add_action( 'wp_ajax_' . self::AJAX_UPLOAD_IMAGE, [$this, 'ajaxUploadPassportImage'] );
        add_action( 'wp_ajax_nopriv_' . self::AJAX_UPLOAD_IMAGE, [$this, 'ajaxUploadPassportImage'] );

        add_action( 'wp_ajax_' . self::AJAX_DELETE_IMAGE, [$this, 'ajaxDeletePassportImage'] );
        add_action( 'wp_ajax_nopriv_' . self::AJAX_DELETE_IMAGE, [$this, 'ajaxDeletePassportImage'] );

        add_filter( 'woocommerce_admin_order_item_thumbnail', [$this, 'uniqProductThumbInOrderDetails'], 10, 3 );
    }

    public function uniqProductThumbInOrderDetails( $image, $itemId, $item )
    {
        $customImage = wc_get_order_item_meta( $itemId, 'Upload passport image', false );
        if ( ! empty( $customImage ) ) {
            return '<img width="150" height="150" class="attachment-thumbnail size-thumbnail" loading="lazy" src="' . $customImage[0] . '">';
        }

        return $image;
    }

    /**
     * Enable regular and sale price for product
     */
    public function enableRegPriceForPassportProduct()
    {
        if ( get_post_type() != 'product' ) {
            return;
        }

        wc_enqueue_js(
          "jQuery(document).ready(function () {
                jQuery('.options_group.pricing').addClass('show_if_passport').show();
                jQuery('.inventory_options').addClass('show_if_passport').show();
                jQuery('#inventory_product_data ._sku_field').addClass('hide_if_passport').hide();
            });"
        );
    }

    /**
     * Add custom product settings fields
     */
    public function productAddCustomSettings()
    {
        echo '<div class="options_group"></div>';
    }

    public function ajaxUploadPassportImageOriginalSize()
    {
        $postedData = isset( $_POST ) ? $_POST : [];
        $fileData   = isset( $_FILES ) ? $_FILES : [];
        $data       = array_merge( $postedData, $fileData );
        $error      = false;

        try {
            $type = pathinfo( $data['image']['name'] );

            if ( ! in_array( strtolower($type['extension']), ['jpg', 'jpeg', 'gif', 'png'] ) ) {
                throw new Exception( esc_html__( 'Invalid image type', 'custom-checkout-plugin' ) );
            }

            $fileName     = "passport_" . time() . '.' . $type['extension'];
            $uploadDir    = wp_get_upload_dir();
            $filePath     = $uploadDir['path'] . '/' . $fileName;
            $uploadedFile = move_uploaded_file( $data['image']['tmp_name'], $filePath );

            if( ! $uploadedFile ) {
                throw new Exception( esc_html__( 'Did not match data URI with image data', 'custom-checkout-plugin' ) );
            }

        } catch ( Exception $e ) {
            $error = $e->getMessage();
        }

        $response = [];
        if ( $error === false ) {
            $response['response'] = esc_html__( 'Success', 'custom-checkout-plugin' );
            $response['filename'] = $fileName;
            $response['url'] = $uploadDir['url'] . '/' . $fileName;
            $response['type'] = $type['extension'];
            echo json_encode( $response );
            die();
        }

        $response['response'] = esc_html__( 'Error', 'custom-checkout-plugin' );
        $response['error'] = $error;
        echo json_encode( $response );
        die();
    }

    public function ajaxUploadPassportImage()
    {
        $postedData = isset( $_POST ) ? $_POST : [];
        $fileData = isset( $_FILES ) ? $_FILES : [];
        $data = array_merge( $postedData, $fileData );
        $error = false;

        try {
            if ( preg_match( '/^data:image\/(\w+);base64,/', $data['image'], $type ) ) {
                $data = substr( $data['image'], strpos( $data['image'], ',' ) + 1 );
                $type = strtolower( $type[1] );

                if ( ! in_array( $type, ['jpg', 'jpeg', 'gif', 'png'] ) ) {
                    throw new Exception( esc_html__( 'Invalid image type', 'custom-checkout-plugin' ) );
                }
                $data = str_replace( ' ', '+', $data );
                $data = base64_decode( $data );

                if ( $data === false ) {
                    throw new Exception( esc_html__( 'base64_decode failed', 'custom-checkout-plugin' ) );
                }
            } else {
                throw new Exception( esc_html__( 'Did not match data URI with image data', 'custom-checkout-plugin' ) );
            }
        } catch ( Exception $e ) {
            $error = $e->getMessage();
        }

        $fileName = "passport_" . time() . ".{$type}";
        $uploadDir = wp_get_upload_dir();
        $filePath = $uploadDir['path'] . '/' . $fileName;
        $uploadedFile = file_put_contents( $filePath, $data );

        $response = [];
        if ( $uploadedFile !== false ) {
            $response['response'] = esc_html__( 'Success', 'custom-checkout-plugin' );
            $response['filename'] = $fileName;
            $response['url'] = $uploadDir['url'] . '/' . $fileName;
            $response['type'] = $type;
            echo json_encode( $response );
            die();
        }

        $response['response'] = esc_html__( 'Error', 'custom-checkout-plugin' );
        $response['error'] = $error;
        echo json_encode( $response );
        die();
    }

    public function ajaxDeletePassportImage()
    {
        $uploadDir = wp_get_upload_dir();

        if ( file_exists( $uploadDir['path'] . '/' . basename( $_POST['imgSrc'] ) ) ) {
            unlink( $uploadDir['path'] . '/' . basename( $_POST['imgSrc'] ) );
        }

        die();
    }
}